import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-movie-generic-form',
  templateUrl: './movie-generic-form.component.html',
  styleUrls: ['./movie-generic-form.component.css']
})
export class MovieGenericFormComponent {

  
  @Output() updateEvent=new EventEmitter();

  submit(event:any){
this.updateEvent.emit(event.value);
  }
 
}
