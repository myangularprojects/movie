import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent {

  // storageForms:any=new FormGroup({
  //   storageType:new FormControl('',[Validators.required,Validators.minLength(3)]),
  //   storageAccessKey:new FormControl('',[Validators.required,Validators.minLength(8)]),
  //   storageSecretKey:new FormControl('',[Validators.required,Validators.minLength(8)]),
  //   storageBucketName:new FormControl('',[Validators.required,Validators.minLength(5)]),
  //   storageRegion:new FormControl('',[Validators.required,Validators.minLength(3)])

  // });

  storageForm:any;

  costructor(private fb:FormBuilder){
    this.storageForm=this.fb.group({
      storageType:['',[Validators.required,Validators.minLength(3)]],
      storageAccessKey:['',[Validators.required,Validators.minLength(8)]],
      storageSecretKey:['',[Validators.required,Validators.minLength(8)]],
      storageBucketName:['',[Validators.required,Validators.minLength(5)]],
      storageRegion:['',[Validators.required,Validators.minLength(3)]]
    });
  }
  saveStorageForm(){
    console.log(this.storageForm);
  }

  getStorageForm(controlName:any){
    return this.storageForm.controls.controlName;
  }

}
