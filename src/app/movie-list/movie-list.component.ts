import { ColDef } from 'ag-grid-community';

import { Component, Input,OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnChanges {
@Input() public movieObj:any;
 public movieArray:any=[]
public columnDefs: ColDef[] = [
  {field:"moviename"},
  {field:"actor"},
  {field:"actress"},
  {field:"movierating"}
];

public defaultColDef: ColDef = {
  sortable: true,
  filter: true,
};

ngOnChanges() {
  console.log(this.movieObj);
  this.movieArray.push(this.movieObj)
  this.movieArray=[...this.movieArray];
}
}
