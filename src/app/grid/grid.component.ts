import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnChanges {
  @Input() gridColumnList:any;
  @Input() gridDataList:any;
  @Input() gridConfig:any;

  ngOnChanges(){
    console.log(this.gridColumnList);
  }
}
